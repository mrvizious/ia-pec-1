# PEC 1 - IA para juegos en primera persona

## Actividad 1

Para esta actividad se ha seguido el tutorial adjuntado como recurso de la actividad. El código se ha modificado ligeramente según gustos personales. El script de navegación del agente es el siguiente:

```csharp
using UnityEngine;
using UnityEngine.AI;

public class AgentMovementScript : MonoBehaviour
{
    //Identifier for the mouse button to use. Public so it can be changed in the inspector
    public int mouseButton = 0;

    private NavMeshAgent agent;
    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }
    void Update()
    {
        //Perform actions if the mouse button is clicked.
        //0 = Left, 1 = Right
        if (Input.GetMouseButtonDown(mouseButton))
        {
            //Variable to store hit information from the raycast
            RaycastHit hit;

            //Ray from the camera to the mouse position
            Ray rayFromCamera = Camera.main.ScreenPointToRay(Input.mousePosition);

            //If the ray hit a walkable surface, set it as the target for the agent
            if (Physics.Raycast(rayFromCamera, out hit, 100))
            {
                if (hit.transform.tag.Equals("Floor"))
                {
                    agent.destination = hit.point;
                }
            }
        }
    }
}
```

Dado que el NavMeshAgent iba a estar en el mismo GameObject que el script AgentMovement, he decidido cambiar la visibilidad del NavMeshAgent a _public_ y obtener una referencia en el método ```Start()```.

El archivo de vídeo del ejemplo se llama **Actividad1.mkv**, y el UnityPackage se llama **Actividad1.unitypackage**.

## Actividad 2

En esta actividad se quiere crear un comportamiento de movimiento tipo _wanderer_, en el que un agente "pasea" por el mapeado. Para ello, se empleará una técnica que se halla en el capítulo 6.5 del libro _The Nature of Code_, de Daniel Shiffman.

Para crear un patrón de movimiento convincente, cada poco tiempo, especificado como una variable, se buscará el siguiente punto al que ir, se validará, y añadirá como destino al agente del NavMesh de Unity.

Este siguiente punto se elige proyectando un punto enfrente del agente a una distancia especificada por variable, y a partir de ahí se elige un punto a otra distancia también especificada por variable, a un ángulo aleatorio. Esta segunda distancia debería ser menor que la primera, para que el punto que se elija siempre esté deante del agente y, por tanto, el destino que se elija esté siempre delante del agente, dando la impresión de un movimiento más natural.

El código utilizado para esto es el siguiente:

```csharp
using UnityEngine;
using UnityEngine.AI;

public class WanderingScript : MonoBehaviour
{
    private NavMeshAgent agent;

    //Public variables that control how far the center of the search will be
    //and how far from that center the point will be chosen
    [Min(0)]
    public float seekCenterDistance = 5f, seekRadius = 3f;

    //Public variable to set how long should the agent wait to do the next
    //position check
    [Min(0)]
    public float secondsPerCheck = 0.05f;
    private float secondsSinceLastCheck = 0f;


    //Public variables that control the maximum depth trying to find a suitable
    //point with the parameters given
    [Min(0)]
    public int maxNumberOfTries = 200;

    //Set to true in the inspector to draw Debug information
    public bool drawGizmos = false;


    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }
    void Update()
    {
        //Adds the ime since the last frame to the variable that controls how
        //often the checks should be made
        secondsSinceLastCheck += Time.deltaTime;

        //If the time since the last check is greater or equal to the specified
        //cadency, reset the time between checks and check
        if (secondsSinceLastCheck >= secondsPerCheck)
        {
            secondsSinceLastCheck = 0f;
            SeekNextPosition();
        }
    }

    /// <summary>
    /// Tries to find an appropriate next point to follow and, if it can't, it
    /// sets the point just behind the agent
    /// </summary>
    private void SeekNextPosition()
    {
        // Find center for circle from the floor
        Vector3 seekCenter =
            transform.position - new Vector3(0, transform.position.y, 0)
            + transform.forward * seekCenterDistance;

        //Show debug ray pointing to the center in red if the debug options are set
        if (drawGizmos) Debug.DrawRay(transform.position, seekCenter
                                        - transform.position, Color.red);

        // Declaration of variables for loop control and data to infinity
        Vector3 newExactTarget = Vector3.positiveInfinity;
        Vector3 newInexactTarget = Vector3.positiveInfinity;

        //Set counter for maximum depth of search
        int depthCounter = 0;

        //Loop that searches for a calid point
        while (newExactTarget.x == Mathf.Infinity)
        {
            //Sets a temporary "inexact" point using a random angle
            newInexactTarget =
                seekCenter
                + Quaternion.AngleAxis(Random.Range(0, 360), transform.up)
                * transform.forward * seekRadius;

            //Tries to get a valid "exact" point from the "inexact" point
            newExactTarget = ConvertInexactTargetToExactTarget(newInexactTarget);

            //Checks if the depth is already too deep
            //If it is, it breaks out of the loop and sets the target point
            //directly behind the agent
            if (depthCounter >= maxNumberOfTries)
            {

                //Logs error of depth
                Debug.LogError("Too much recursion!");

                //Set target behind agent
                newExactTarget = -transform.forward * 0.1f;

                //Break out of while loop
                break;
            }

            //If debug options are set, draw a ray from the center to the inexact
            //center that has been selected, and paint it white if it is valid or
            //red if it isn't
            if (drawGizmos)
            {
                Debug.DrawRay(seekCenter, newInexactTarget - seekCenter,
                                newExactTarget.x == Mathf.Infinity ?
                                Color.red : Color.white);
            }

            //Update depth counter
            depthCounter++;
        }

        //Officially set target so the agent follows it
        setTarget(newExactTarget);
    }

    /// <summary>
    /// Tries to find a valid target point within 0.1f distance of the given point.
    /// </summary>
    /// <param name="newInexactTarget"></param>
    /// <returns>Valid point or Vector3.PositiveInfinity
    /// if no valid point is found</returns>
    private Vector3 ConvertInexactTargetToExactTarget(Vector3 newInexactTarget)
    {
        NavMeshHit hit;
        NavMesh.SamplePosition(newInexactTarget, out hit, 0.1f, NavMesh.AllAreas);
        return hit.position;
    }
    /// <summary>
    /// Sets the input position as the target for the agent
    /// </summary>
    /// <param name="newExactTarget"></param>
    public void setTarget(Vector3 newExactTarget)
    {
        agent.destination = newExactTarget;
    }
}

```

El archivo de vídeo del ejemplo se llama **Actividad2.mkv**, y el UnityPackage se llama **Actividad2.unitypackage**.

## Actividad 3

La actividad 3 propone crear un sistema de visión para el agente que permita ver en un ángulo concreto a una distancia determinada, y que dé algún indicativo visual si ve al jugador.

En este caso, vamos a dibujar un cono de visión que salga de la altura de los ojos del agente, se expanda los ángulos determinados por una variable y se aleje tanto como se le indique por otra. Para ello, haremos uso del tipo de objeto Mesh que existe en Unity.

Para construir el Mesh, hay que crear los vértices, triángulos, y normales, así que creamos una función individual para cada una de estas tareas, de manera que el código quede algo más ordenado.

Los vértices, dado que se tratan de un cono, se definen según el ángulo, la distancia, y el número de vértices exteriores especificados. Todo esto se obtiene de variables que se pueden modificar. Para localizar el punto concreto de un vértice, se calcula el lugar ideal del vértice y se realiza un raycast hasta él. Si se encuentra un objeto con el tag "Obstacle", que corresponde a muros y todo aquello a través de lo que no se pueda ver, se pone el vértice justo en ese sitio. Si algún raycast se encuentra un jugador, guarda la información para notificar que se ha encontrado.

Los triángulos, a su vez, se conforman por el vértice 0, que corresponde con el inicio del cono, y dos vértices seguidos. De esta manera los triángulos son 0, 1, 2; 0, 2, 3; 0, 3, 4...

Las normales son todas iguales, y corresponden con el Vector3.up.

Después de crear el cono, se le actualiza el color: Rojo si se ha encontrado un jugador, amarillo si no.

```csharp
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PerceptionScript : MonoBehaviour
{

    //Cone topology: alpha of the color, height from which it should be emmited
    public float coneAlpha = 0.7f, coneHeight = 0.65f;
    //Cone topology: *Total* degrees of vision, distance of vision, number of vertices
    public int coneAngleDegrees = 15, coneDistance = 3, coneMeshVertices = 200;


    //Last position where the player was seen
    public Vector3 lastSeenPlayerPosition;
    //Game Object belonging to the last character that has been seen
    public GameObject latestSeenPlayer;

    //Custom event to notify of whenever the player has been found
    public UnityEvent onPlayerFound = new UnityEvent();


    //Variable that checks if the player has been found in the testing frame
    private bool foundPlayerInFrame = false;
    private MeshRenderer meshRenderer;
    private Mesh mesh;
    private MeshFilter meshFilter;


    private void Start()
    {
        meshRenderer = gameObject.GetComponent<MeshRenderer>();
        meshFilter = gameObject.GetComponent<MeshFilter>();
        mesh = new Mesh();
    }

    void Update()
    {
        //Every frame, the cone position and shape is updated, as well as a
        //check to find a player
        UpdateConeOfVisionMesh();
        //Depending on whether the player was found or not, the color of the
        //cone changes
        UpdateConeColor();
        //If true, notify any script that needs it that a player was found
        //this frame
        if (foundPlayerInFrame) onPlayerFound.Invoke();

    }

    /// <summary>
    /// Wrapper method that updates all the geometry for the cone of vision and
    /// checks if a player has been found
    /// </summary>
    private void UpdateConeOfVisionMesh()
    {
        //First, the mesh is cleared as per recommendations of Unity:
        //https://docs.unity3d.com/ScriptReference/Mesh.html (Point 3)
        mesh.Clear();

        //Calculate all of the geeometry with different specific methods
        CalculateConeMeshPoints();
        CalculateConeMeshTriangles();
        CalculateConeMeshNormals();

        //Replace current mesh with new one
        meshFilter.mesh = mesh;
    }

    /// <summary>
    /// Calculates and assigns the points of the cone of vision mesh to the mesh.
    /// </summary>
    /// <returns></returns>
    private List<Vector3> CalculateConeMeshPoints()
    {
        //List of vertices of the mesh
        List<Vector3> vertices = new List<Vector3>();

        //Add vertex on agent
        vertices.Add(new Vector3(0, coneHeight, 0));

        //Transform angle from degrees to radians
        float coneAngleRadians = coneAngleDegrees * (Mathf.PI / 180f);
        //Get initial angle so the center of the cone equals the forward of the agent
        float startingConeAngleRadians = -(coneAngleDegrees * (Mathf.PI / 180f)) / 2;

        //Reset to false the variable that checks if the player was found in the frame
        foundPlayerInFrame = false;

        //Loop to populate the vertices list
        for (int i = 0; i <= coneMeshVertices; i++)
        {
            //Calculate current coordinates of radius
            float currentConeAngleRadians = startingConeAngleRadians
                                    + i * (coneAngleRadians / coneMeshVertices);
            float x = Mathf.Cos(currentConeAngleRadians) * coneDistance;
            float y = Mathf.Sin(currentConeAngleRadians) * coneDistance;

            //Check if the player is found or if there is a wall between
            //the point and the agent
            vertices.Add(CheckConePoint(new Vector3(y, coneHeight, x)));
        }

        //Assign the array to the mesh vertices
        mesh.vertices = vertices.ToArray();

        return vertices;
    }

    /// <summary>
    /// Calculates and assigns the triangles to the mesh
    /// </summary>
    /// <returns></returns>
    private List<int> CalculateConeMeshTriangles()
    {
        //List of the vertex numbers that make the triangles
        List<int> triangles = new List<int>();

        //Creates all the triangles, using always the agent vertex (0) as
        //the center, and two consecutive vertices one after the other
        for (int i = 1; i < mesh.vertices.Length - 1; i++)
        {
            triangles.Add(0);
            triangles.Add(i);
            triangles.Add(i + 1);
        }
        //Assign triangles to mesh
        mesh.triangles = triangles.ToArray();

        return triangles;
    }

    /// <summary>
    /// Calculates and assigns the normals to the mesh
    /// </summary>
    /// <returns></returns>
    private List<Vector3> CalculateConeMeshNormals()
    {
        //List of normals to add to the mesh
        List<Vector3> normals = new List<Vector3>();

        //Adds to all of the vertices a normal pointing up
        for (int i = 0; i < mesh.vertices.Length; i++)
        {
            normals.Add(Vector3.up);
        }

        //Assign the normals to the mesh
        mesh.normals = normals.ToArray();

        return normals;
    }

    /// <summary>
    /// Changes the color of the cone according to whether the player was found or not
    /// </summary>
    private void UpdateConeColor()
    {
        //Prepare Color variable to be used
        Color newColor;

        //Change the color depending on whether the player has been found or not
        if (foundPlayerInFrame)
        {
            newColor = Color.red;
            newColor.a = coneAlpha;
        }
        else
        {
            newColor = Color.yellow;
            newColor.a = coneAlpha;
        }
        meshRenderer.material.color = newColor;
    }

    /// <summary>
    /// Returns a correct point for the possiblePoint given, depending on a
    /// few conditions.
    /// If a player is found, the variable foundPlayerInFrame is set to true
    /// so it can be processed later
    /// </summary>
    /// <param name="possiblePoint"></param>
    /// <returns>Local point with correct position for the point given</returns>
    private Vector3 CheckConePoint(Vector3 possiblePoint)
    {
        //Hit variable to store raycast info
        RaycastHit hit;

        //Raycast coming from the position of the agent to the possiblePoint
        //with the distance of the cone
        if (Physics.Raycast(transform.position
                            ,transform.TransformPoint(possiblePoint)
                            - transform.position
                            ,out hit, coneDistance))
        {
            //If the raycast hits an obstacle, return the local position of the hit
            if (hit.transform.tag.Equals("Obstacle"))
            {
                return transform.InverseTransformPoint(hit.point);
            }
            //If the raycast finds a player, update the foundPlayerInFrame to true
            //After that, it raycasts again behind that
            if (hit.transform.tag.Equals("Player"))
            {
                latestSeenPlayer = hit.transform.gameObject;
                if (foundPlayerInFrame == true)
                {
                    lastSeenPlayerPosition = hit.transform.position;
                }
                //Set variable foundPlayerInFrame to true so it can be processed later
                else foundPlayerInFrame = true;
                //Create layerMask so player layer doesn't interfere
                LayerMask ignorePlayerLayerMask = 1 << LayerMask.NameToLayer("Player");
                ignorePlayerLayerMask = ~ignorePlayerLayerMask;
                if (Physics.Raycast(transform.position
                                    , transform.TransformPoint(possiblePoint)
                                    - transform.position
                                    , out hit, coneDistance
                                    , ignorePlayerLayerMask))
                {
                    if (hit.transform.tag.Equals("Obstacle"))
                    {
                        //If there is an obstacle behind the player, return
                        //that collision point
                        return transform.InverseTransformPoint(hit.point);
                    }
                }
            }
        }

        //If no obstacle is found, return original possiblePoint
        return possiblePoint;
    }
}

```

El archivo de vídeo del ejemplo se llama **Actividad3.mkv**, y el UnityPackage se llama **Actividad3.unitypackage**.

## Actividad 4

En esta actividad se pedía implementar una máquina de estados que dependiendo de las situaciones, hiciese que el agente se comportase de maneras diferentes. Vamos a definir los estados que se han implementado y cómo:
![Datos](Informe Data/State Flow Chart.png "Tabla de flujo de estados")

En la tabla se puede observar el comportamiento que tendrá el agente al encontrarse con el jugador, además de las transiciones que tendrá. Empezará paseando hasta que vea al jugador, en cuyo momento comenzará a perseguirle. Se le atrapa, tendrá un comportamiento similar al de paseo, pero no podrá volver a perseguir al jugador hasta que haya alcanzado una distancia con el jugador igual a la que se le haya establecido en una variable.

Si, en cambio, no consigue atrapar al jugador, llegará hasta el último punto conocido en el que estaba y volverá a pasear.

El archivo de vídeo del ejemplo se llama **Actividad4.mkv**, y el UnityPackage se llama **Actividad4.unitypackage**.

### AgentStateMachine

Para estos estados, se ha creado una clase que haga de máquina de estados y maneje la transición entre unos y otros:

```csharp
using UnityEngine;

public class AgentStateMachine : MonoBehaviour
{
    private AgentState currentState;

    private void Start()
    {
        SetState(gameObject.AddComponent<WanderState>());
    }

    public void SetState(AgentState newState)
    {
        if (currentState != null) Destroy(currentState);
        currentState = newState;
        currentState.Begin(this);
    }
}
```

Como se puede ver, cada vez que se cambia de estado, se destruye el anterior y se llama al método ```Begin()``` del nuevo.

### AgentState

Aquí tenemos el método abstracto del que heredan los diferentes estados:

```csharp
using UnityEngine;

public abstract class AgentState : MonoBehaviour
{
    protected AgentStateMachine agentStateMachine;

    public virtual void Begin(AgentStateMachine newAgentStateMachine)
    {
        agentStateMachine = newAgentStateMachine;
    }

    protected virtual void ChangeAgentColor(Color newColor)
    {
        gameObject.GetComponent<MeshRenderer>().material.color = newColor;
    }

}
```

### WanderState

Este es el estado en que el agente pasea por el mapa sin rumbo fijo hasta que encuentre al jugador.

```csharp
using UnityEngine;

public class WanderState : AgentState
{
    private WanderingScript wanderingScript;

    /// <summary>
    /// Starts the Wander routine
    /// </summary>
    /// <param name="newAgentStateMachine"></param>
    public override void Begin(AgentStateMachine newAgentStateMachine)
    {
        //Logging information
        Debug.Log("Beginning Wander State!");

        //Call generic state Begin()
        base.Begin(newAgentStateMachine);

        //Changing color to indicate the player the current state
        base.ChangeAgentColor(Color.white);

        //Adds a WanderingScript to create movement
        wanderingScript = gameObject.AddComponent<WanderingScript>();

        //Sets a listener so, if the agent finds the player, it changes state to chase
        GetComponentInChildren<PerceptionScript>().onPlayerFound.AddListener(
            () => agentStateMachine.SetState(gameObject.AddComponent<ChaseState>())
            );
    }

    /// <summary>
    /// If destroyed, take away the WanderScript
    /// </summary>
    private void OnDestroy()
    {
        Destroy(wanderingScript);
    }
}
```

### ChaseState

Este es el estado en que el agente ha visto al jugador y le perseguirá hasta capturarle o hasta que llegue al último punto donde sabe que estuvo antes de perderlo de vista.

```csharp
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class ChaseState : AgentState
{

    private NavMeshAgent agent;
    private PerceptionScript perceptionScript;
    private GameObject latestSeenPlayer;

    public float reachDistance = 5f;


    /// <summary>
    /// Starts the Chase routine
    /// </summary>
    /// <param name="newAgentStateMachine"></param>
    public override void Begin(AgentStateMachine newAgentStateMachine)
    {
        //Logging information
        Debug.Log("Beginning Chase State!", this);

        //Call generic state Begin()
        base.Begin(newAgentStateMachine);

        //Changing color to indicate the player the current state
        base.ChangeAgentColor(Color.red);

        //Gets necessary components
        agent = GetComponent<NavMeshAgent>();
        perceptionScript = GetComponentInChildren<PerceptionScript>();

        //Sets destination to the position where the player was seen last
        agent.destination = perceptionScript.lastSeenPlayerPosition;
    }

    private void Update()
    {
        //Sets destination to the position where the player was seen last
        agent.destination = perceptionScript.lastSeenPlayerPosition;

        //Keeps track of the player that was seen last so it can check if it
        //captures it or not
        latestSeenPlayer = perceptionScript.latestSeenPlayer;

        //Calculate how close it can get to the player
        reachDistance = (agent.radius
                        + perceptionScript.latestSeenPlayer
                        .GetComponent<NavMeshAgent>().radius) * 1.5f;

        if (agent.remainingDistance <= reachDistance)
        {
            //Calculates the distance between the player and the agent
            float distanceToPlayer = Mathf.Abs(
                                        Vector3.Distance(
                                            latestSeenPlayer.transform.position
                                            , transform.position
                                        )
                                    );

            //Creating LayerMask that only includes player
            int playerLayerMask = 1 << LayerMask.NameToLayer("Player");
            playerLayerMask = ~playerLayerMask;

            //If the player is closer than the reach distance, it means that the
            //agent reached the player, so it raises onPlayerCaptured event and
            //change to Escape state
            if (Physics.CheckSphere(transform.position, reachDistance, playerLayerMask))
            {
                onPlayerCaptured.Invoke();
                ChangeToEscape();
            }
            //If the player is farther than the reach distance, it means it escaped
            //and it changes the state to Wander
            else
            {
                ChangeToWander();
            }
        }
    }

    /// <summary>
    /// Change state to Wander
    /// </summary>
    private void ChangeToWander()
    {
        AgentState wanderState = gameObject.AddComponent<WanderState>();
        agentStateMachine.SetState(wanderState);
    }

    /// <summary>
    /// Change state to Escape
    /// </summary>
    private void ChangeToEscape()
    {
        EscapeState escapeState = gameObject.AddComponent<EscapeState>();
        escapeState.setPlayer(latestSeenPlayer);
        agentStateMachine.SetState(escapeState);
    }
}
```

### EscapeState

Este es el estado en que el agente se escapa del jugador, y lo hace paseando hasta alejarse una distancia concreta especificada por variable. Una vez se ha alejado lo suficiente, pasa a pasear pudiendo volver a perseguirle.

```csharp
using UnityEngine;

public class EscapeState : AgentState
{

    //Distance that the target has to get away from the player
    public float escapeDistance = 10f;

    //Player the agent is trying to run away from
    private GameObject player;
    private WanderingScript wanderingScript;

    /// <summary>
    /// Starts the Escape routine
    /// </summary>
    /// <param name="newAgentStateMachine"></param>
    public override void Begin(AgentStateMachine newAgentStateMachine)
    {
        //Logging information
        Debug.Log("Beginning Escape State!");

        //Call generic state Begin()
        base.Begin(newAgentStateMachine);

        //Changing color to indicate the player the current state
        base.ChangeAgentColor(Color.magenta);

        //Adds a WanderingScript to create movement
        wanderingScript = gameObject.AddComponent<WanderingScript>();
    }

    public void setPlayer(GameObject newPlayer)
    {
        player = newPlayer;
    }

    private void Update()
    {
        //If the agent is far enough from the player, change into Wander State
        if (Mathf.Abs(
            Vector3.Distance(player.transform.position, transform.position)
            )>= escapeDistance)
        {
            agentStateMachine.SetState(gameObject.AddComponent<WanderState>());
        }
        //TODO: If it happened to be a multiplayer game, a check to see if the
        //new player that is seen by the perception script is the same as the
        //one it is escaping from should be implemented and, if it isn't, go and
        //chase it
    }

    /// <summary>
    /// If destroyed, take away the WanderScript
    /// </summary>
    private void OnDestroy()
    {
        Destroy(wanderingScript);
    }
}
```

## Actividad 5 - Juego

Para este punto, he decidido hacer un juego en el que, poco a poco, vayan apareciendo más y más agentes que intenten atraparte. Tu objetivo es escapar de ellos e intentar sobrevivir el máximo tiempo posible. Cada poco tiempo, además, irán apareciendo distintos power ups que te proporcionarán mejoras:

- El amarillo te proporciona velocidad extra de movimiento durante un tiempo determinado.
- El azul ralentiza a los agentes durante un tiempo determinado.
- El gris hace que, durante un tiempo, los agentes vean con menor distancia y menor ángulo.
- El rojo es una bomba, y destruye a todos los agentes que encuentre en su radio.

El archivo de vídeo del ejemplo se llama **Actividad5.mkv**, y el UnityPackage se llama **Actividad5.unitypackage**.
