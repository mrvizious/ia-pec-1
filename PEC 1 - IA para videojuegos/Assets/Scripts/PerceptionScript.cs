﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PerceptionScript : MonoBehaviour
{

    //Cone topology: alpha of the color, height from which it should be emmited
    public float coneAlpha = 0.7f, coneHeight = 0.65f;
    //Cone topology: *Total* degrees of vision, distance of vision, number of vertices 
    public int coneAngleDegrees = 15, coneDistance = 3, coneMeshVertices = 200;


    //Last position where the player was seen
    public Vector3 lastSeenPlayerPosition;
    //Game Object belonging to the last character that has been seen
    public GameObject latestSeenPlayer;

    //Custom event to notify of whenever the player has been found
    public UnityEvent onPlayerFound = new UnityEvent();


    //Variable that checks if the player has been found in the testing frame
    private bool foundPlayerInFrame = false;
    private MeshRenderer meshRenderer;
    private Mesh mesh;
    private MeshFilter meshFilter;


    private void Start()
    {
        meshRenderer = gameObject.GetComponent<MeshRenderer>();
        meshFilter = gameObject.GetComponent<MeshFilter>();
        mesh = new Mesh();
    }

    void Update()
    {
        //Every frame, the cone position and shape is updated, as well as a 
        //check to find a player
        UpdateConeOfVisionMesh();
        //Depending on whether the player was found or not, the color of the 
        //cone changes 
        UpdateConeColor();
        //If true, notify any script that needs it that a player was found 
        //this frame
        if (foundPlayerInFrame) onPlayerFound.Invoke();

    }

    /// <summary>
    /// Wrapper method that updates all the geometry for the cone of vision and 
    /// checks if a player has been found
    /// </summary>
    private void UpdateConeOfVisionMesh()
    {
        //First, the mesh is cleared as per recommendations of Unity:
        //https://docs.unity3d.com/ScriptReference/Mesh.html (Point 3)
        mesh.Clear();

        //Calculate all of the geeometry with different specific methods
        CalculateConeMeshPoints();
        CalculateConeMeshTriangles();
        CalculateConeMeshNormals();

        //Replace current mesh with new one
        meshFilter.mesh = mesh;
    }

    /// <summary>
    /// Calculates and assigns the points of the cone of vision mesh to the mesh.
    /// </summary>
    /// <returns></returns>
    private List<Vector3> CalculateConeMeshPoints()
    {
        //List of vertices of the mesh
        List<Vector3> vertices = new List<Vector3>();

        //Add vertex on agent
        vertices.Add(new Vector3(0, coneHeight, 0));

        //Transform angle from degrees to radians
        float coneAngleRadians = coneAngleDegrees * (Mathf.PI / 180f);
        //Get initial angle so the center of the cone equals the forward of the agent
        float startingConeAngleRadians = -(coneAngleDegrees * (Mathf.PI / 180f)) / 2;

        //Reset to false the variable that checks if the player was found in the frame
        foundPlayerInFrame = false;

        //Loop to populate the vertices list
        for (int i = 0; i <= coneMeshVertices; i++)
        {
            //Calculate current coordinates of radius
            float currentConeAngleRadians = startingConeAngleRadians
                                    + i * (coneAngleRadians / coneMeshVertices);
            float x = Mathf.Cos(currentConeAngleRadians) * coneDistance;
            float y = Mathf.Sin(currentConeAngleRadians) * coneDistance;

            //Check if the player is found or if there is a wall between 
            //the point and the agent
            vertices.Add(CheckConePoint(new Vector3(y, coneHeight, x)));
        }

        //Assign the array to the mesh vertices
        mesh.vertices = vertices.ToArray();

        return vertices;
    }

    /// <summary>
    /// Calculates and assigns the triangles to the mesh
    /// </summary>
    /// <returns></returns>
    private List<int> CalculateConeMeshTriangles()
    {
        //List of the vertex numbers that make the triangles
        List<int> triangles = new List<int>();

        //Creates all the triangles, using always the agent vertex (0) as 
        //the center, and two consecutive vertices one after the other
        for (int i = 1; i < mesh.vertices.Length - 1; i++)
        {
            triangles.Add(0);
            triangles.Add(i);
            triangles.Add(i + 1);
        }
        //Assign triangles to mesh
        mesh.triangles = triangles.ToArray();

        return triangles;
    }

    /// <summary>
    /// Calculates and assigns the normals to the mesh
    /// </summary>
    /// <returns></returns>
    private List<Vector3> CalculateConeMeshNormals()
    {
        //List of normals to add to the mesh
        List<Vector3> normals = new List<Vector3>();

        //Adds to all of the vertices a normal pointing up
        for (int i = 0; i < mesh.vertices.Length; i++)
        {
            normals.Add(Vector3.up);
        }

        //Assign the normals to the mesh
        mesh.normals = normals.ToArray();

        return normals;
    }

    /// <summary>
    /// Changes the color of the cone according to whether the player was found or not
    /// </summary>
    private void UpdateConeColor()
    {
        //Prepare Color variable to be used
        Color newColor;

        //Change the color depending on whether the player has been found or not
        if (foundPlayerInFrame)
        {
            newColor = Color.red;
            newColor.a = coneAlpha;
        }
        else
        {
            newColor = Color.yellow;
            newColor.a = coneAlpha;
        }
        meshRenderer.material.color = newColor;
    }

    /// <summary>
    /// Returns a correct point for the possiblePoint given, depending on a 
    /// few conditions. 
    /// If a player is found, the variable foundPlayerInFrame is set to true 
    /// so it can be processed later
    /// </summary>
    /// <param name="possiblePoint"></param>
    /// <returns>Local point with correct position for the point given</returns>
    private Vector3 CheckConePoint(Vector3 possiblePoint)
    {
        //Hit variable to store raycast info
        RaycastHit hit;

        //Raycast coming from the position of the agent to the possiblePoint 
        //with the distance of the cone
        if (Physics.Raycast(transform.position
                            , transform.TransformPoint(possiblePoint)
                            - transform.position
                            , out hit, coneDistance))
        {
            //If the raycast hits an obstacle, return the local position of the hit
            if (hit.transform.tag.Equals("Obstacle"))
            {
                return transform.InverseTransformPoint(hit.point);
            }
            //If the raycast finds a player, update the foundPlayerInFrame to true
            //After that, it raycasts again behind that
            if (hit.transform.tag.Equals("Player"))
            {
                latestSeenPlayer = hit.transform.gameObject;
                if (foundPlayerInFrame == true)
                {
                    lastSeenPlayerPosition = hit.transform.position;
                }
                //Set variable foundPlayerInFrame to true so it can be processed later
                else foundPlayerInFrame = true;
                //Create layerMask so player layer doesn't interfere
                LayerMask ignorePlayerLayerMask = 1 << LayerMask.NameToLayer("Player");
                ignorePlayerLayerMask = ~ignorePlayerLayerMask;
                if (Physics.Raycast(transform.position
                                    , transform.TransformPoint(possiblePoint)
                                    - transform.position
                                    , out hit, coneDistance
                                    , ignorePlayerLayerMask))
                {
                    if (hit.transform.tag.Equals("Obstacle"))
                    {
                        //If there is an obstacle behind the player, return 
                        //that collision point
                        return transform.InverseTransformPoint(hit.point);
                    }
                }
            }
        }

        //If no obstacle is found, return original possiblePoint
        return possiblePoint;
    }
}
