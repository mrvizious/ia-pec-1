﻿using UnityEngine;

public class WanderState : AgentState
{
    private WanderingScript wanderingScript;

    /// <summary>
    /// Starts the Wander routine
    /// </summary>
    /// <param name="newAgentStateMachine"></param>
    public override void Begin(AgentStateMachine newAgentStateMachine)
    {
        //Logging information
        Debug.Log("Beginning Wander State!", this);

        //Call generic state Begin()
        base.Begin(newAgentStateMachine);

        //Changing color to indicate the player the current state
        base.ChangeAgentColor(Color.white);

        //Adds a WanderingScript to create movement
        wanderingScript = gameObject.AddComponent<WanderingScript>();

        //Sets a listener so, if the agent finds the player, it changes state to chase
        GetComponentInChildren<PerceptionScript>().onPlayerFound.AddListener(
            () => agentStateMachine.SetState(gameObject.AddComponent<ChaseState>())
            );
    }

    /// <summary>
    /// If destroyed, take away the WanderScript
    /// </summary>
    private void OnDestroy()
    {
        Destroy(wanderingScript);
    }
}
