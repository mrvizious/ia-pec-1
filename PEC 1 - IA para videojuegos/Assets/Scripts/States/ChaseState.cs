﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class ChaseState : AgentState
{

    private NavMeshAgent agent;
    private PerceptionScript perceptionScript;
    private GameObject latestSeenPlayer;

    public float reachDistance = 5f;


    /// <summary>
    /// Starts the Chase routine
    /// </summary>
    /// <param name="newAgentStateMachine"></param>
    public override void Begin(AgentStateMachine newAgentStateMachine)
    {
        //Logging information
        Debug.Log("Beginning Chase State!", this);

        //Call generic state Begin()
        base.Begin(newAgentStateMachine);

        //Changing color to indicate the player the current state
        base.ChangeAgentColor(Color.red);

        //Gets necessary components
        agent = GetComponent<NavMeshAgent>();
        perceptionScript = GetComponentInChildren<PerceptionScript>();

        //Sets destination to the position where the player was seen last
        agent.destination = perceptionScript.lastSeenPlayerPosition;
    }

    private void Update()
    {
        //Sets destination to the position where the player was seen last
        agent.destination = perceptionScript.lastSeenPlayerPosition;

        //Keeps track of the player that was seen last so it can check if it
        //captures it or not
        latestSeenPlayer = perceptionScript.latestSeenPlayer;

        //Calculate how close it can get to the player
        reachDistance = (agent.radius
                        + perceptionScript.latestSeenPlayer
                        .GetComponent<NavMeshAgent>().radius) * 1.5f;

        if (agent.remainingDistance <= reachDistance)
        {
            //Calculates the distance between the player and the agent
            float distanceToPlayer = Mathf.Abs(
                                        Vector3.Distance(
                                            latestSeenPlayer.transform.position
                                            , transform.position
                                        )
                                    );

            //Creating LayerMask that only includes player
            int playerLayerMask = 1 << LayerMask.NameToLayer("Player");
            playerLayerMask = ~playerLayerMask;

            //If the player is closer than the reach distance, it means that the
            //agent reached the player, so it raises onPlayerCaptured event and
            //change to Escape state
            if (Physics.CheckSphere(transform.position, reachDistance, playerLayerMask))
            {
                onPlayerCaptured.Invoke();
                ChangeToEscape();
            }
            //If the player is farther than the reach distance, it means it escaped
            //and it changes the state to Wander
            else
            {
                ChangeToWander();
            }
        }
    }

    /// <summary>
    /// Change state to Wander
    /// </summary>
    private void ChangeToWander()
    {
        AgentState wanderState = gameObject.AddComponent<WanderState>();
        agentStateMachine.SetState(wanderState);
    }

    /// <summary>
    /// Change state to Escape
    /// </summary>
    private void ChangeToEscape()
    {
        EscapeState escapeState = gameObject.AddComponent<EscapeState>();
        escapeState.setPlayer(latestSeenPlayer);
        agentStateMachine.SetState(escapeState);
    }
}
