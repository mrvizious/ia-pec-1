﻿using UnityEngine;
using UnityEngine.Events;

public abstract class AgentState : MonoBehaviour
{
    protected AgentStateMachine agentStateMachine;
    public UnityEvent onPlayerCaptured = new UnityEvent();

    public virtual void Begin(AgentStateMachine newAgentStateMachine)
    {
        agentStateMachine = newAgentStateMachine;
    }

    protected virtual void ChangeAgentColor(Color newColor)
    {
        gameObject.GetComponent<MeshRenderer>().material.color = newColor;
    }

}
