﻿using UnityEngine;

public class EscapeState : AgentState
{

    //Distance that the target has to get away from the player
    public float escapeDistance = 10f;

    //Player the agent is trying to run away from
    private GameObject player;
    private WanderingScript wanderingScript;

    /// <summary>
    /// Starts the Escape routine
    /// </summary>
    /// <param name="newAgentStateMachine"></param>
    public override void Begin(AgentStateMachine newAgentStateMachine)
    {
        //Logging information
        Debug.Log("Beginning Escape State!", this);

        //Call generic state Begin()
        base.Begin(newAgentStateMachine);

        //Changing color to indicate the player the current state
        base.ChangeAgentColor(Color.magenta);

        //Adds a WanderingScript to create movement
        wanderingScript = gameObject.AddComponent<WanderingScript>();
    }

    public void setPlayer(GameObject newPlayer)
    {
        player = newPlayer;
    }

    private void Update()
    {
        //If the agent is far enough from the player, change into Wander State
        if (Mathf.Abs(
            Vector3.Distance(player.transform.position, transform.position)
            ) >= escapeDistance)
        {
            agentStateMachine.SetState(gameObject.AddComponent<WanderState>());
        }
        //TODO: If it happened to be a multiplayer game, a check to see if the
        //new player that is seen by the perception script is the same as the
        //one it is escaping from should be implemented and, if it isn't, go and
        //chase it
    }

    /// <summary>
    /// If destroyed, take away the WanderScript
    /// </summary>
    private void OnDestroy()
    {
        Destroy(wanderingScript);
    }
}