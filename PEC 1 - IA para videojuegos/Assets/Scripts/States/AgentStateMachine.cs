﻿using UnityEngine;
using UnityEngine.Events;

public class AgentStateMachine : MonoBehaviour
{
    private AgentState currentState;
    public UnityEvent onPlayerCaptured = new UnityEvent();

    private void Start()
    {
        SetState(gameObject.AddComponent<WanderState>());
    }

    public void SetState(AgentState newState)
    {
        if (currentState != null) Destroy(currentState);
        currentState = newState;
        currentState.Begin(this);
        //Subscribe to the new State's onPlayerCaptured event
        currentState.onPlayerCaptured.AddListener(() => InvokePlayerCaptured());
    }

    /// <summary>
    /// Invokes the onPlayerCaptured event
    /// </summary>
    private void InvokePlayerCaptured()
    {
        onPlayerCaptured.Invoke();
    }
}