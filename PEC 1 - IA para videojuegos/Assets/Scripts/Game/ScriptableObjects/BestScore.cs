﻿using UnityEngine;

[CreateAssetMenu(fileName = "BestScore", menuName = "ScriptableObjects/BestScore", order = 1)]
public class BestScore : ScriptableObject
{
    public float bestTime;

    public bool ProposeNewBestTime(float newPossibleBestTime)
    {
        if (newPossibleBestTime > bestTime)
        {
            bestTime = newPossibleBestTime;
            return true;
        }
        return false;
    }
}
