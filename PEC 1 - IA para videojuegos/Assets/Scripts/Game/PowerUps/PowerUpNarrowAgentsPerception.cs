﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class PowerUpNarrowAgentsPerception : PowerUp
{
    private int previousAgentAngle, previousAgentDistance;
    public override void Begin(Spawner newSpawner, NavMeshAgent newPlayer)
    {
        base.Begin(newSpawner, newPlayer);

        ChangePowerUpColor(Color.grey);

    }

    protected override IEnumerator PowerUpCoroutine()
    {
        if (spawner.agents != null && spawner.agents.Count > 0)
        {
            previousAgentAngle = spawner.agents[0].GetComponentInChildren<PerceptionScript>().coneAngleDegrees;
            previousAgentDistance = spawner.agents[0].GetComponentInChildren<PerceptionScript>().coneDistance;
            foreach (GameObject agent in spawner.agents)
            {
                agent.GetComponentInChildren<PerceptionScript>().coneAngleDegrees = (int)(agent.GetComponentInChildren<PerceptionScript>().coneAngleDegrees * 0.3f);
                agent.GetComponentInChildren<PerceptionScript>().coneDistance = (int)(agent.GetComponentInChildren<PerceptionScript>().coneDistance * 0.4f);
            }

            yield return new WaitForSeconds(secondsDuration);

            foreach (GameObject agent in spawner.agents)
            {
                agent.GetComponentInChildren<PerceptionScript>().coneAngleDegrees = previousAgentAngle;
                agent.GetComponentInChildren<PerceptionScript>().coneDistance = previousAgentDistance;
            }
        }
        Destroy(gameObject);
    }
}