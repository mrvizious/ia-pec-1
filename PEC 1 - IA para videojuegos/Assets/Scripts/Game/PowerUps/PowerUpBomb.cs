﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class PowerUpBomb : PowerUp
{
    public float explosionRadius = 25f;
    public override void Begin(Spawner newSpawner, NavMeshAgent newPlayer)
    {
        base.Begin(newSpawner, newPlayer);

        rotationSpeed *= 10;
        ChangePowerUpColor(Color.red);

    }

    protected override IEnumerator PowerUpCoroutine()
    {
        int agentsLayerMask = 1 << LayerMask.NameToLayer("Enemy");

        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        Destroy(sphere.GetComponent<Collider>());
        sphere.GetComponent<MeshRenderer>().material.color = Color.red;
        sphere.transform.localScale = new Vector3(explosionRadius, explosionRadius, explosionRadius);
        sphere.transform.position = transform.position;

        int layerMaskAgents = 1 << LayerMask.NameToLayer("Enemy");

        Collider[] hitColliders = Physics.OverlapSphere(player.transform.position, explosionRadius, layerMaskAgents);
        foreach (Collider collider in hitColliders)
        {
            Destroy(collider.gameObject);
        }

        yield return new WaitForSeconds(0.5f);
        Destroy(sphere);
        Destroy(gameObject);
    }
}
