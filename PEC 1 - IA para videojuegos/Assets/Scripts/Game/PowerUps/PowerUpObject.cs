﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public enum PowerUpTypes
{
    ExtraPlayerSpeed,
    SlowDownAgents,
    NarrowAgentsPerception,
    Bomb
}
public class PowerUpObject : MonoBehaviour
{
    public Spawner spawner;
    public NavMeshAgent player;
    private PowerUp powerUp;
    private MeshRenderer meshRenderer;
    private Collider triggerCollider;
    private void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        triggerCollider = GetComponent<Collider>();
        RandomPowerUp();
    }

    private void RandomPowerUp()
    {
        switch ((PowerUpTypes)Random.Range(0, System.Enum.GetValues(typeof(PowerUpTypes)).Length))
        {
            case PowerUpTypes.ExtraPlayerSpeed:
                powerUp = gameObject.AddComponent<PowerUpExtraPlayerSpeed>();
                break;
            case PowerUpTypes.SlowDownAgents:
                powerUp = gameObject.AddComponent<PowerUpSlowDownAgents>();
                break;
            case PowerUpTypes.NarrowAgentsPerception:
                powerUp = gameObject.AddComponent<PowerUpNarrowAgentsPerception>();
                break;
            case PowerUpTypes.Bomb:
                powerUp = gameObject.AddComponent<PowerUpBomb>();
                break;
            default: break;
        }
        powerUp.Begin(spawner, player);
    }

    //private IEnumerator DelayOnBegin()
    //{
    //    yield return new WaitForSeconds(0.1f);
    //    powerUp.Begin(spawner, player);
    //}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            meshRenderer.enabled = false;
            triggerCollider.enabled = false;
            powerUp.Activate();
        }
    }
}
