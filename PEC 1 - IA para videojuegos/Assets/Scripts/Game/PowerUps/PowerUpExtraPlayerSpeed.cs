﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class PowerUpExtraPlayerSpeed : PowerUp
{
    private float previousPlayerSpeed, previousPlayerAngularSpeed;
    public override void Begin(Spawner newSpawner, NavMeshAgent newPlayer)
    {
        base.Begin(newSpawner, newPlayer);

        rotationSpeed *= 30;
        ChangePowerUpColor(Color.yellow);

    }

    protected override IEnumerator PowerUpCoroutine()
    {
        previousPlayerSpeed = player.speed;
        previousPlayerAngularSpeed = player.angularSpeed;
        player.speed *= 1.5f;
        player.angularSpeed *= 50f;

        yield return new WaitForSeconds(secondsDuration);

        player.speed = previousPlayerSpeed;
        player.angularSpeed = previousPlayerAngularSpeed;
        Destroy(gameObject);
    }
}
