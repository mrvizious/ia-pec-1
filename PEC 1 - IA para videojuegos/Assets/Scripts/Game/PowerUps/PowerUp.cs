﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public abstract class PowerUp : MonoBehaviour
{
    protected Spawner spawner;
    protected NavMeshAgent player;
    public float rotationSpeed = 50f, secondsDuration = 10f;
    private void Update()
    {
        transform.rotation = Quaternion.Euler(0f, transform.rotation.eulerAngles.y + rotationSpeed * Time.deltaTime, 0f);
    }
    public virtual void Begin(Spawner newSpawner, NavMeshAgent newPlayer)
    {
        Debug.Log("Beginning!");
        spawner = newSpawner;
        player = newPlayer;
    }

    public void Activate()
    {
        StartCoroutine(PowerUpCoroutine());
    }

    protected abstract IEnumerator PowerUpCoroutine();
    protected virtual void ChangePowerUpColor(Color newColor)
    {
        gameObject.GetComponent<MeshRenderer>().material.color = newColor;
    }
}
