﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class PowerUpSlowDownAgents : PowerUp
{
    private float previousAgentSpeed, previousAgentAngularSpeed;
    public override void Begin(Spawner newSpawner, NavMeshAgent newPlayer)
    {
        base.Begin(newSpawner, newPlayer);

        ChangePowerUpColor(Color.blue);

    }

    protected override IEnumerator PowerUpCoroutine()
    {
        if (spawner.agents != null && spawner.agents.Count > 0)
        {
            previousAgentSpeed = spawner.agents[0].GetComponent<NavMeshAgent>().speed;
            previousAgentAngularSpeed = spawner.agents[0].GetComponent<NavMeshAgent>().angularSpeed;
            foreach (GameObject agent in spawner.agents)
            {
                agent.GetComponent<NavMeshAgent>().speed *= 0.6f;
                agent.GetComponent<NavMeshAgent>().angularSpeed *= 0.6f;
            }

            yield return new WaitForSeconds(secondsDuration);

            foreach (GameObject agent in spawner.agents)
            {
                agent.GetComponent<NavMeshAgent>().speed = previousAgentSpeed;
                agent.GetComponent<NavMeshAgent>().angularSpeed = previousAgentSpeed;
            }
        }
        Destroy(gameObject);
    }
}