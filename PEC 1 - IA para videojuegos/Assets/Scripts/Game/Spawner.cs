﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class Spawner : MonoBehaviour
{
    public GameObject agentPrefab;
    public GameObject powerUpPrefab;
    public NavMeshAgent player;
    public BestScore bestScore;
    [Min(0f)]
    public float secondsBetweenAgentSpawns = 5f, secondsBetweenPowerUpSpawns = 5f, spawnFreeRadius = 5f;
    [Min(0)]
    public int maximumNumberOfTries = 200;
    public float secondsUntilNextAgentSpawn, totalSeconds = 0f;
    public UnityEvent onPlayerCaptured = new UnityEvent();
    private float secondsSinceLastAgentSpawn = 0f, secondsSinceLastPowerUpSpawn = 0f;
    private IEnumerator spawnAgentCoroutine;
    private IEnumerator spawnPowerUpCoroutine;
    public List<GameObject> agents;
    private GameObject agentsEmptyObject, powerUpsEmptyObject;

    private void Start()
    {
        agentsEmptyObject = new GameObject("Agents");
        powerUpsEmptyObject = new GameObject("PowerUps");
        powerUpPrefab.GetComponent<PowerUpObject>().spawner = this;
        powerUpPrefab.GetComponent<PowerUpObject>().player = player;
    }

    public void StartSpawningAgents()
    {
        Debug.Log("Agent spawner started!");
        if (spawnAgentCoroutine == null)
        {
            spawnAgentCoroutine = SpawnAgentCoroutine();
            StartCoroutine(spawnAgentCoroutine);
        }
    }
    public void StartSpawningPowerUps()
    {
        Debug.Log("PowerUp spawner started!");
        if (spawnPowerUpCoroutine == null)
        {
            spawnPowerUpCoroutine = SpawnPowerUpCoroutine();
            StartCoroutine(spawnPowerUpCoroutine);
        }
    }

    public void StopSpawningAgents()
    {
        if (spawnAgentCoroutine != null)
        {
            StopCoroutine(spawnAgentCoroutine);
            spawnAgentCoroutine = null;
        }
    }
    public void StopSpawningPowerUps()
    {
        if (spawnPowerUpCoroutine != null)
        {
            StopCoroutine(spawnPowerUpCoroutine);
            spawnPowerUpCoroutine = null;
        }
    }

    IEnumerator SpawnAgentCoroutine()
    {
        while (true)
        {
            secondsSinceLastAgentSpawn += Time.deltaTime;
            secondsUntilNextAgentSpawn = secondsBetweenAgentSpawns - secondsSinceLastAgentSpawn;
            totalSeconds += Time.deltaTime;
            bestScore.ProposeNewBestTime(totalSeconds);

            if (secondsSinceLastAgentSpawn >= secondsBetweenAgentSpawns)
            {
                if (Spawn(agentPrefab)) secondsSinceLastAgentSpawn = 0f;
            }

            agents.RemoveAll(item => item == null);
            yield return null;
        }
    }

    IEnumerator SpawnPowerUpCoroutine()
    {

        while (true)
        {
            secondsSinceLastPowerUpSpawn += Time.deltaTime;

            if (secondsSinceLastPowerUpSpawn >= secondsBetweenPowerUpSpawns)
            {

                if (Spawn(powerUpPrefab)) secondsSinceLastPowerUpSpawn = 0f;
            }

            yield return null;
        }
    }

    private bool Spawn(GameObject objectToSpawn)
    {
        bool spawnPerformed = false;
        for (int i = 0; !spawnPerformed && i < maximumNumberOfTries; i++)
        {
            float x = Random.Range(-17f, 17f);
            float z = Random.Range(-17f, 17f);

            int notSpawnLayerMask = 1 << LayerMask.NameToLayer("Player") | 1 << LayerMask.NameToLayer("Obstacle") | 1 << LayerMask.NameToLayer("Enemy") | 1 << LayerMask.NameToLayer("PowerUp");
            if (!Physics.CheckSphere(new Vector3(x, 1f, z), spawnFreeRadius, notSpawnLayerMask))
            {
                if (objectToSpawn.GetComponent<NavMeshAgent>())
                {
                    if (agents == null) agents = new List<GameObject>();
                    agents.Add(Instantiate(agentPrefab, new Vector3(x, 1f, z), Quaternion.Euler(0f, Random.Range(0f, 360f), 0f), agentsEmptyObject.transform));
                    agents[agents.Count - 1].GetComponent<AgentStateMachine>().onPlayerCaptured.AddListener(() => onPlayerCaptured.Invoke());
                }
                else
                {
                    GameObject powerUpObject = Instantiate(objectToSpawn, new Vector3(x, 1f, z), Quaternion.Euler(0f, Random.Range(0f, 360f), 0f), powerUpsEmptyObject.transform);
                }
                return true;
            }
        }
        return spawnPerformed;
    }
    public void DestroyAllAgents()
    {
        if (agents != null)
        {
            foreach (GameObject agent in agents)
            {
                Destroy(agent);
            }
        }
    }
    private void OnDestroy()
    {
        DestroyAllAgents();
    }
}