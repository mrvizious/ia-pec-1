﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public BestScore bestScore;
    public GameManager gameManager;
    public Spawner spawner;
    public Text bestTime, currentTime, secondsUntilNextSpawn, livesLeft;

    private void Update()
    {
        bestTime.text = "Best Time: " + bestScore.bestTime.ToString("#.00");
        currentTime.text = "Current Time: " + spawner.totalSeconds.ToString("#.00");
        if (Mathf.Floor((int)spawner.secondsUntilNextAgentSpawn) <= 3)
        {
            secondsUntilNextSpawn.color = Color.red;
        }
        else
        {
            secondsUntilNextSpawn.color = Color.black;
        }
        secondsUntilNextSpawn.text = "Next Spawn in " + (int)spawner.secondsUntilNextAgentSpawn;
        livesLeft.text = "Lives left: " + gameManager.livesLeft;
    }
}
