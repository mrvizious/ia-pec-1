﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private Spawner spawner;
    public int livesLeft = 3;

    private void Start()
    {
        spawner = GetComponent<Spawner>();
        spawner.onPlayerCaptured.AddListener(() => SubstractLife());
        spawner.StartSpawningAgents();
        spawner.StartSpawningPowerUps();
    }

    private void SubstractLife()
    {
        livesLeft--;
        if (livesLeft <= 0)
        {
            LoseGame();
        }
    }

    private void LoseGame()
    {
        SceneManager.LoadScene("EndScreen");
    }
}
