﻿using UnityEngine;
using UnityEngine.AI;

public class AgentMovementByClickScript : MonoBehaviour
{
    //Identifier for the mouse button to use. Public so it can be changed in the inspector
    public int mouseButton = 0;

    private NavMeshAgent agent;
    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }
    void Update()
    {
        //Perform actions if the mouse button is clicked.
        //0 = Left, 1 = Right
        if (Input.GetMouseButtonDown(mouseButton))
        {
            //Variable to store hit information from the raycast
            RaycastHit hit;

            //Ray from the camera to the mouse position
            Ray rayFromCamera = Camera.main.ScreenPointToRay(Input.mousePosition);

            //If the ray hit a walkable surface, set it as the target for the agent
            if (Physics.Raycast(rayFromCamera, out hit, 100))
            {
                if (hit.transform.tag.Equals("Floor") || hit.transform.tag.Equals("PowerUp"))
                {
                    agent.destination = hit.point;
                }
            }
        }
    }
}
